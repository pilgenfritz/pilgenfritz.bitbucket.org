CKEDITOR.plugins.setLang(
	'doksoft_preview',
	'en',
	{	
        doksoft_image_title: "Inserir Imagem",  // image
        doksoft_preview_title: "Upload de Imagem", // preview
        doksoft_file_title: "Inserir arquivo", // file
        doksoft_gallery_title: "Inserir Galeria", // gallery
        doksoft_bootstrap_gallery_title: "Inserir galeria Bootstrap", // bs gallery

        doksoft_bootstrap_gallery_col_count: "columns",

        file_filter_name: "Todos os tipos suportados",

        resize: "Tam Máx.", // image or preview
        resize_up: "Scale up if small image", // image or preview
        resize_thumb: "Thumbnail", // preview
        download_label: "Download", // file
        status_file_upload: "Enviando...",
        status_file_upload_success: "Upload concluído. Clique em OK para continuar.",
        status_file_upload_wait: "Enviando arquivos. Aguarde...",
        status_file_upload_fail: "Não foi possível enviaro o arquivo. Tente com outro arquivo.",
        label_send_to_server: "Enviar para o servidor.",
        label_no_file: "Você não enviou o arquivo.",
        label_no_support: "Carregando..."
	}
);
